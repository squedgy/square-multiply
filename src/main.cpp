#include "argparse/argparse.hpp"
#include "main.hpp"
#include "square-multiply.hpp"
#include "time.h"
#include <chrono>
#include "math.h"

namespace {
	void unbranched(uint32_t base, uint32_t exponent, uint32_t dividend) {
		uint64_t remainder = sqmul::remainderOf(base, exponent, dividend);
		std::cout
			<< "[Base " << std::to_string(base) << "]"
			<< "[Expo " << std::to_string(exponent) << "]"
			<< "[Divi " << std::to_string(dividend) << "]: "
			<< std::to_string(remainder)
			<< std::endl;
	}

	void branched(uint32_t base, uint32_t exponent, uint32_t dividend) {
		uint64_t remainder = sqmul::remainderOfBranched(base, exponent, dividend);
		std::cout
			<< "[Base " << std::to_string(base) << "]"
			<< "[Expo " << std::to_string(exponent) << "]"
			<< "[Divi " << std::to_string(dividend) << "]: "
			<< std::to_string(remainder)
			<< std::endl;
	}
}

int main(int argc, char* argv[]) {
	argparse::ArgumentParser program("square-multiply");

	program.add_argument("base").scan<'i', uint32_t>();
	program.add_argument("exponent").scan<'i', uint32_t>();
	program.add_argument("dividend").scan<'i', uint32_t>();

	try {
		program.parse_args(argc, argv);
	} catch (const std::runtime_error& err) {
		std::cerr << err.what() << std::endl;
		std::cerr << program;
		std::exit(1);
	}

	auto base = program.get<uint32_t>("base");
	auto exponent = program.get<uint32_t>("exponent");
	auto dividend = program.get<uint32_t>("dividend");

	branched(base, exponent, dividend);
	unbranched(base, exponent, dividend);

//	std::chrono::time_point<std::chrono::high_resolution_clock> start;
//	std::chrono::time_point<std::chrono::high_resolution_clock> end;

//	start = std::chrono::high_resolution_clock::now();
//	branched(3, 45, 7);
//	branched(23, 373, 747);
//	branched(4568, 55486, 2556);
//	branched(89, 0xFFF0, 1112);
//	branched(2225, 44456, 555856);
//	branched(1 << 31, 0xF0000FF0, 0xFF0000FE);
//	end = std::chrono::high_resolution_clock::now();
//	std::cout << "  Branched took " << (std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count()) << std::endl;

//	start = std::chrono::high_resolution_clock::now();
//	unbranched(3, 45, 7);
//	unbranched(23, 373, 747);
//	unbranched(4568, 55486, 2556);
//	unbranched(89, 0xFFF0, 1112);
//	unbranched(2225, 44456, 555856);
//	unbranched(1 << 31, 0xF0000FF0, 0xFF0000FE);
//	end = std::chrono::high_resolution_clock::now();
//	std::cout << "Unbranched took " << (std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count()) << std::endl;

//	start = std::chrono::high_resolution_clock::now();
//	branched(3, 45, 7);
//	branched(23, 373, 747);
//	branched(4568, 55486, 2556);
//	branched(89, 0xFFF0, 1112);
//	branched(2225, 44456, 555856);
//	branched(1 << 31, 0xF0000FF0, 0xFF0000FE);
//	end = std::chrono::high_resolution_clock::now();
//	std::cout << "  Branched took " << (std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count()) << std::endl;
//
//	start = std::chrono::high_resolution_clock::now();
//	unbranched(3, 45, 7);
//	unbranched(23, 373, 747);
//	unbranched(4568, 55486, 2556);
//	unbranched(89, 0xFFF0, 1112);
//	unbranched(2225, 44456, 555856);
//	unbranched(1 << 31, 0xF0000FF0, 0xFF0000FE);
//	end = std::chrono::high_resolution_clock::now();
//	std::cout << "Unbranched took " << (std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count()) << std::endl;
//
//	start = std::chrono::high_resolution_clock::now();
//	uint64_t thing = ((uint64_t) pow(1 << 31, 0xF0000FF0)) % 0xFF0000FE;
//	std::cout << "Math.pow " << std::to_string(thing) << std::endl;
//	end = std::chrono::high_resolution_clock::now();
//	std::cout << "Math.pow took " << (std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count()) << std::endl;

	return 0;
}