#include "square-multiply.hpp"

namespace {
   // Branchless
	uint32_t max(uint32_t a) {
		bool gt = a > 0;
        return gt * a + !gt;
	}
}

uint64_t sqmul::remainderOf(uint32_t base, uint32_t exponent, uint32_t dividend) {
	if(exponent == 0) return 1;

	uint8_t targetBit = 31;
	// Find the most significant bit
	for(
		;
		targetBit > 0 &&
		(exponent & (1 << targetBit)) == 0;
		targetBit--
	);

	uint64_t ret = base; // base ^ 1(binary)
	targetBit--;

	for(; targetBit != 0; targetBit--) {
		// shift operation left
		ret *= ret; // base * {binary number}0(binary)
		// keep number relatively small
		ret %= dividend; // adjust
		// multiply by base IF the exponents targetBit is 1
		ret *= max( // set the bit to 0 or 1
			// if the exponent's targetBit is set (1) this should be base
			// otherwise this is 0
			((exponent & (1 << targetBit)) > 0) * base
		);
		ret %= dividend;
	}

	// shift operation left
	ret *= ret;
	// keep number relatively small
	ret %= dividend;
	// multiply by base IF the exponents targetBit is 1
	ret *= max(
		// if the exponent's targetBit is set (1) this should be base
		// otherwise this is 0
		((exponent & 1) > 0) * base
	);
	ret %= dividend;

	return ret;
}

uint64_t sqmul::remainderOfBranched(uint32_t base, uint32_t exponent, uint32_t dividend) {
	if(exponent == 0) return 1;

	uint8_t targetBit = 31;
	// Find the most significant bit
	for(
		;
		targetBit >= 0 &&
		(exponent & (1 << targetBit)) == 0;
		targetBit--
	);

	uint64_t ret = base; // base ^ 1(binary)
	targetBit--;

	for(; targetBit != 0; targetBit--) {
		// shift operation left
		ret *= ret; // base * {binary number}0(binary)
		// keep number relatively small
		ret %= dividend; // adjust
		if (exponent & (1 << targetBit)) {
			ret *= base;
			ret %= dividend;
		}
	}

	// shift operation left
	ret *= ret;
	// keep number relatively small
	ret %= dividend;
	if (exponent & (1 << targetBit)) {
		ret *= base;
		ret %= dividend;
	}

	return ret;
}

