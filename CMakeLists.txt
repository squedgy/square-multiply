cmake_minimum_required(VERSION 3.18)
project(square-multiply LANGUAGES CXX)
set(CMAKE_CXX_STANDARD 20)

# Define our library files
add_executable(${CMAKE_PROJECT_NAME}
	${CMAKE_CURRENT_SOURCE_DIR}/src/square-multiply.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/src/main.cpp
)

list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmake")
include(os_utils)

include_directories(
	"${CMAKE_CURRENT_SOURCE_DIR}/include",
	"${CMAKE_CURRENT_SOURCE_DIR}/externals/argparse/include"
)
target_include_directories(${CMAKE_PROJECT_NAME}
	PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include"
)

# If there's a given OUTPUT_DIR use it
if($ENV{OUTPUT_DIR})
	set(OUT_DIR $ENV{OUTPUT_DIR})
	# Make sure it ends with a file path separator
	if(NOT ${OUT_DIR} MATCHES "${P_SEP}$")
		set(OUT_DIR "${OUT_DIR}${P_SEP}")
	endif()
	set(OUTPUT ${OUT_DIR}build/${os_id})
else() # Otherwise just target <SOURCE_DIR>/build/{win|lin|mac}/
	set(OUTPUT ${CMAKE_SOURCE_DIR}/build/${os_id}/)
endif()

set_target_properties(${CMAKE_PROJECT_NAME} PROPERTIES ARCHIVE_OUTPUT_DIRECTORY ${OUTPUT}/$<0:>)
set_target_properties(${CMAKE_PROJECT_NAME} PROPERTIES LIBRARY_OUTPUT_DIRECTORY ${OUTPUT}/$<0:>)
set_target_properties(${CMAKE_PROJECT_NAME} PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${OUTPUT}/$<0:>)