# Square Multiply

An implementation of the square-multiply algorithm that I learned about while watching
[this video](https://www.youtube.com/watch?v=cbGB__V8MNk) from Computerphile.

It takes in 3 arguments treats them as the parts of the equation `Math.pow(x, y) % z` and should work with any three 32-bit numbers

# Getting Started

```shell
# Retrieve repository
git clone https://gitlab.com/squedgy/square-multiply.git
# OR
git clone git@gitlab.com:squedgy/square-multiply.git # if you use ssh with gitlab

# Pull submodules
git submodule init
git submodule update --recursive
```

### This project _should_ work in Visual Studio
##### otherwise

```shell
# You will need cmake and (by default) GNU make
cmake .
make

# Depending on the platform you're on the executable should be in a sub-directory of build
# Either win, lin, or mac
# example is on Ubuntu 21.10
build/lin/square-multiply 23 373 747
# Output
# [Base 23][Expo 373][Divi 747]: 131
# [Base 23][Expo 373][Divi 747]: 131
```

The first output line is the algorithm without using an `if` statement to determine whether to adjust the current bit's
truthiness. When the "current bit" is `1` it should multiply by the base (x), essentially.

The second line uses an `if` for the decision metionend above.

# Executable Arguments
`square-multiply [x] [y] [z]`