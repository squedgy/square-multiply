#pragma once
#include <stdint.h>

namespace sqmul {
	uint64_t remainderOf(uint32_t base, uint32_t exponent, uint32_t divider);
	uint64_t remainderOfBranched(uint32_t base, uint32_t exponent, uint32_t divider);
}